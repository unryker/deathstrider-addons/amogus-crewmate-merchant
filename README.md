### Notes
---
- Adds the crewmate from Among Us as a merchant skin for Deathstrider.
- The crewmate appears in a variety of random colors.

### Credits
---
Graphical Assets:
- Pillowblaster
	- Crewmate

Death Sound:
- Among Us

Music:
- Leonz
	- Among Us (Trap Remix)

Special Thanks:
- Accensus
	- The crewmate was pretty much done already thanks to him, even down to the colors. It's a quick and dirty port from the old [Reloading Room.](https://gitlab.com/accensi/hd-addons/reloading-room)